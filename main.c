/*
  Blue Pill template code - working stdio usart
  polprog 
  april 2020

  docs:
  http://libopencm3.org/docs/latest/stm32f1/html/
 */

#ifndef STM32F1
#define STM32F1
#endif


#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/usart.h>

#include "uart.h"

#include <stdio.h>
#include <stdlib.h>



int main(){


  rcc_periph_clock_enable(RCC_GPIOA);
  
  rcc_periph_clock_enable(RCC_USART1);
  usart_setup();
  
  rcc_periph_clock_enable(RCC_GPIOC);

  //rcc_periph_clock_enable(RCC_GPIOB);
  //rcc_periph_clock_enable(RCC_AFIO); //enable AFIO clock to be able to remap

  gpio_set_mode(GPIOC, GPIO_MODE_OUTPUT_2_MHZ,
		GPIO_CNF_OUTPUT_PUSHPULL, GPIO13);
  
  //Unlock PB3 (JTAG disable), unlock PB15 (remap timer1)
  //gpio_primary_remap(AFIO_MAPR_SWJ_CFG_JTAG_OFF_SW_ON,
  //                   AFIO_MAPR_TIM1_REMAP_FULL_REMAP);


  
  int z = 0;
  while(1){

      for (int i = 0; i < 150000; i++) __asm__("nop");
      gpio_toggle(GPIOC, GPIO13);
      printf("%d\n", z);
      z++;
  }
}
