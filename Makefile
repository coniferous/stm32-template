#this makefile is helped in part by https://github.com/libopencm3/libopencm3-examples/blob/master/examples/Makefile.rules

########################################
# based on rue's makefile
# small edits added by polprog for his conveinience
# update 19.03.2020 polprog: updated to work with 2 yrs worth of changes
#                   in locm3 the project dir is set up so that OPENCM3_DIR
#                   is a symlink to the actual locm3 dir cloned from github.
#                   You have to build that first ofc.
# update 01.04.2020 polprog: added -mcpu=cortex-m3 -mthumb to LDFLAGS as well
#                   to make sure newlib functions are being linked in with the
#                   correct instruction encoding
#
# The contents of this file are public domain work.
########################################



#what is the main file 
BINARY = program
#OBJS = $(BINARY).o $(HL_LIBS:.c=.o)

CFILES = $(wildcard *.c)
HFILES = $(wildcard *.h)
OBJS = $(CFILES:%.c=%.o)

#LibopenCM3 location
OPENCM3_DIR = libopencm3
SRCLIBDIR = $(OPENCM3_DIR)
INCLUDE_DIR = $(OPENCM3_DIR)/include
LIB_DIR = $(OPENCM3_DIR)/lib


OPENOCD_SCRIPTS_DIR = /usr/share/openocd/scripts

###### Device specific things
DEVICE = stm32f103c8t6

toolchainPath ?= /usr/bin/

PREFIX		?= $(toolchainPath)arm-none-eabi

CC		:= $(PREFIX)-gcc
LD		:= $(PREFIX)-gcc
AR		:= $(PREFIX)-ar
AS		:= $(PREFIX)-as
OBJCOPY		:= $(PREFIX)-objcopy
OBJDUMP		:= $(PREFIX)-objdump
GDB		:= $(PREFIX)-gdb


CFLAGS = 
#with thanks to libopencm3 makefile
CFLAGS += -g
CFLAGS += -Ilibopencm3/include/
CFLAGS += -Wextra -Wshadow -Wimplicit-function-declaration -Wredundant-decls 
CFLAGS += -fno-common -ffunction-sections -fdata-sections
CFLAGS += -MD
CFLAGS += -Wall -Wundef
CFLAGS += -mcpu=cortex-m3 -mthumb
CFLAGS += -T $(LDSCRIPT)

#Includes
CFLAGS += -I $(INCLUDE_DIR)


#######  LD Script #################
# libopencm3 makefile that does the ldscript
#uses libopencm3 to make a .ld file for the specified device
LDFLAGS		?= 
LDFLAGS		+= --static -nostartfiles
LDFLAGS		+= -L$(LIB_DIR)
LDFLAGS		+= -T$(LDSCRIPT)
LDFLAGS		+= -Wl,-Map=$(*).map
LDFLAGS		+= -Wl,--gc-sections


LDLIBS		+= -lopencm3_stm32f1
LDLIBS		+= -Wl,--start-group -lc -lgcc -lnosys -Wl,--end-group 
#include <math.h>
LDLIBS          += -lm -mcpu=cortex-m3 -mthumb


.SUFFIXES: .elf .bin .hex .srec .list .map .images
.SECONDEXPANSION:
.SECONDARY:

all: elf bin

include $(OPENCM3_DIR)/mk/genlink-config.mk
include $(OPENCM3_DIR)/mk/genlink-rules.mk


elf: $(BINARY).elf
bin: $(BINARY).bin
hex: $(BINARY).hex
srec: $(BINARY).srec
list: $(BINARY).list

images: $(BINARY).images
flash: $(BINARY).flash

%.images: %.bin %.hex %.srec %.list %.map
	@#printf "*** $* images generated ***\n"

%.bin: %.elf
	@#printf "  OBJCOPY $(*).bin\n"
	$(OBJCOPY) -Obinary $(*).elf $(*).bin

%.hex: %.elf
	@#printf "  OBJCOPY $(*).hex\n"
	$(OBJCOPY) -Oihex $(*).elf $(*).hex

%.srec: %.elf
	@#printf "  OBJCOPY $(*).srec\n"
	$(OBJCOPY) -Osrec $(*).elf $(*).srec

%.list: %.elf
	@#printf "  OBJDUMP $(*).list\n"
	$(OBJDUMP) -S $(*).elf > $(*).list

%.elf %.map: $(OBJS) $(HFILES) $(LDSCRIPT)
	@#printf "  LD      $(*).elf\n"
	$(LD) $(LDFLAGS) $(OBJS) $(LDLIBS) -o $(BINARY).elf

%.o: %.c
	@#printf "  CC      $(*).c\n"
	$(CC) $(CFLAGS) $(CPPFLAGS) -o $(*).o -c $(*).c

clean:
	@#printf "  CLEAN\n"
	$(RM) $(OBJS)
	$(RM) *.o *.d *.elf *.bin *.hex *.srec *.list *.map


install: program.elf
	openocd -f $(OPENOCD_SCRIPTS_DIR)/interface/stlink-v2.cfg -f $(OPENOCD_SCRIPTS_DIR)/target/stm32f1x.cfg -c init -c targets -c "halt" -c "flash write_image erase ./$(BINARY).elf" -c "verify_image ./$(BINARY).elf" -c "reset run" -c shutdown

reset:		
	openocd -f $(OPENOCD_SCRIPTS_DIR)/interface/stlink-v2.cfg -f $(OPENOCD_SCRIPTS_DIR)/target/stm32f1x.cfg -c init -c targets -c "halt" -c "reset run" -c shutdown


debug: $(BINARY).elf $(BINARY).bin
	openocd -f $(OPENOCD_SCRIPTS_DIR)/interface/stlink-v2.cfg -f $(OPENOCD_SCRIPTS_DIR)/target/stm32f1x.cfg -c init -c targets -c "halt" -c "reset halt" &
	sleep 1; $(GDB) -x armv7m-vecstate-zippe.gdb --eval-command="target remote localhost:3333" $(BINARY).elf

telnet:;
	telnet localhost 4446

testecho:;
	echo $(OPENCM3_DIR)
	echo $(LD_PARAMS)

